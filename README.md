Команды для сборки и запуска:
```
cmake -B ./build/ -DCMAKE_BUILD_TYPE=Release
cmake --build ./build/
build/image-filter input.bmp output.bmp
```

Тест производительности:
```
C version took 0.112025 seconds for 100 images
NASM version took 0.050207 seconds for 100 images
```

Версия на ассемблере в 2.2 раза быстрее

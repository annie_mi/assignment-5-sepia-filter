#include "../headers/bmp.h"
#include <inttypes.h>
#include <stdbool.h>

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool read_header(FILE *file, struct bmp_header *header)
{
    size_t status = fread(header, sizeof(struct bmp_header), 1, file);

    if (status != 1) return false;
    if (header->bfType != 0x4d42) return false;
    if (header->biBitCount != 24) return false;
    if (header->biPlanes != 1) return false;
    if (header->biCompression != 0) return false;
    return fseek(file, header->bOffBits, SEEK_SET) != true;
}

static bool read_pixels(FILE *file, struct image *img, struct bmp_header *header)
{
    fseek(file, header->bOffBits, SEEK_SET);
    allocate_img(img, header->biWidth, header->biHeight);

    uint8_t padding = (4-(header->biWidth*3)%4)%4;
    for(uint32_t i=0; i < header->biHeight; i++){
        size_t count = fread(img->data+i*header->biWidth, sizeof(struct pixel), header->biWidth, file);
        if(count!= header->biWidth){
            return false;
        }
        fseek(file, padding, SEEK_CUR);
    }
    return true;
}

int write_header(FILE *file, struct bmp_header *header, uint32_t width, uint32_t height)
{
    uint8_t padding = (4-(width*3)%4)%4;

    header->bfType = 0x4d42;
    header->bfileSize = sizeof(struct bmp_header) + (width * 3 + padding) * height;
    header->bfileSize = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = 40;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = 1;
    header->biBitCount = 24;
    header->biCompression = 0;
    header->biSizeImage = 0;
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;

    return fwrite(header, sizeof(struct bmp_header), 1, file) != 1;
}

size_t write_pixels(FILE* file, struct image *img, struct bmp_header *header){
    fseek(file, header->bOffBits, SEEK_SET);
    uint8_t padding = (4-(img->width*3)%4)%4;
    for (size_t i = 0; i < img->height; i++) {
        size_t row_size = sizeof(struct pixel) * img->width;
        struct pixel *target_str = img->data + i * img->width;
        if (!fwrite(target_str, row_size, 1, file)) return 1;
        if (padding != 0 && !fwrite(img->data, padding, 1, file)) return 1;
    }
    return 0;
}

enum read_status from_bmp(FILE *file, struct image *img)
{
    struct bmp_header header= {0};
    if (!read_header(file, &header)) return READ_INVALID_HEADER;
    if (!read_pixels(file, img, &header)) return READ_INVALID_BYTES;
    return READ_SUCCESS;
}

enum write_status to_bmp(FILE *file, struct image *img)
{
    struct bmp_header header = {0};
    if (write_header(file, &header, img->width, img->height) != 0) return WRITE_INVALID_HEADER;
    if (write_pixels(file, img, &header) != 0) return WRITE_INVALID_BYTES;
    return WRITE_SUCCESS;
}

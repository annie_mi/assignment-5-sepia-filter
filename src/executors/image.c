#include "../headers/image.h"
#include <malloc.h>
#include <stdint.h>
#include <string.h>

void allocate_img(struct image *img, uint64_t w, uint64_t h) {
    img->width = w;
    img->height = h;
    img->data = malloc(w * h * sizeof(struct pixel));
}

struct image copy_img(struct image *img) {
    struct image res = {0};
    allocate_img(&res, img->width, img->height);
    memcpy(res.data, img->data, img->width * img->height * sizeof(struct pixel));
    return res;
}

void clear_image(struct image *img)
{
    free(img->data);
}


#include "../headers/bmp.h"
#include "../headers/image.h"
#include "../headers/sepia.h"

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

void benchmark(struct image* img, char* name, void (*func)(struct image*)) {
    
    clock_t begin = clock();
    for (int i = 0; i < 100; i++) {
        struct image copy = copy_img(img);
        func(&copy);
    }
    clock_t end = clock();
    float time = (float)(end - begin) / CLOCKS_PER_SEC;

    printf("%s version took %f seconds for 100 images\n", name, time);
}

int main(int argc, char** argv)
{
     if (argc != 3) {
         fprintf(stderr, "Incorrect number of arguments\n");
         return 1;
     }

    FILE* file_input = fopen(argv[1], "rb");
    if (file_input == NULL) {
         fprintf(stderr, "Input file error: %s\n", strerror(errno));
         return 1;
    }

    struct image img = {0};
    
    enum read_status read_status = from_bmp(file_input, &img);
    if(read_status != READ_SUCCESS) {
        switch(read_status){
            case READ_INVALID_HEADER:
                fprintf(stderr, "Bad input file: invalid header\n");
                break;
            case READ_INVALID_BYTES:
                fprintf(stderr, "Bad input file: invalid bytes\n");
            default:
                break;
        }

        clear_image(&img);
        fclose(file_input);
        return 1;
    }
    fclose(file_input);

    benchmark(&img, "C", sepia);
    benchmark(&img, "NASM", sepia_asm);

    sepia_asm(&img);

    FILE* file_output = fopen(argv[2], "wb");
    if (file_output == NULL) {
         fprintf(stderr, "Output file error: %s\n", strerror(errno));
         return 1;
    }

    enum write_status write_status = to_bmp(file_output, &img);
    if(write_status != WRITE_SUCCESS) {
        switch(read_status){
            case WRITE_INVALID_BYTES:
                fprintf(stderr, "Writing error: invalid header\n");
                break;
            case WRITE_INVALID_HEADER:
                fprintf(stderr, "Writing error: invalid bytes\n");
            default:
                break;
        }

        clear_image(&img);
        fclose(file_output);
        return 1;
    }

    return 0;
}

#include "../headers/sepia.h"

void sepia(struct image* img)
{
    uint64_t width = img->height;
    uint64_t height = img->width;

    for (uint64_t x = 0; x < width; x++) {
        for (uint64_t y = 0; y < height; y++) {
            struct pixel* p = &img->data[y * width + x];
            float l = (0.3 * p->r + 0.6 * p->g + 0.1 * p->b) / 255.0;
            p->r = 251 * l;
            p->g = 191 * l;
            p->b = 123 * l;
        }
    }
}

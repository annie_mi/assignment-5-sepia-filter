section .text

global sepia_asm

sepia_asm:
    ; calculate size
    mov rax, [rdi]
    imul rax, [rdi+8]
    imul rax, 3

    ; rcx = current pixel
    mov rcx, [rdi+16]

    ; rax = last address
    add rax, rcx

.loop:
    ; load bgr values
    pmovzxbd xmm0, [rcx]

    ; convert to float
    cvtdq2ps xmm0, xmm0

    ; multiply by luma vector
    mulps xmm0, [.luma_vector]

    ; sum components
    haddps xmm0, xmm0
    haddps xmm0, xmm0

    ; multiply by sepia color
    mulps xmm0, [.sepia_color]

    ; convert to int
    cvtps2dq xmm0, xmm0

    ; store bgr values
    pextrb byte [rcx+0], xmm0, 0
    pextrb byte [rcx+1], xmm0, 4
    pextrb byte [rcx+2], xmm0, 8

    ; go to next iteration
    add rcx, 3
    cmp rcx, rax
    jne .loop

    ret
    align 16
.luma_vector:
	dd 0.1
	dd 0.6
	dd 0.3
	dd 0.0
.sepia_color:
	dd 0.482 ; 123/255
	dd 0.739 ; 191/255
	dd 0.984 ; 251/255
	dd 0.0


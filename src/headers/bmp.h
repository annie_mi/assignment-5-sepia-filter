#pragma once

#include <stdint.h>
#include <stdio.h>

#include "../headers/image.h"


enum read_status
{
        READ_SUCCESS = 0,
        READ_INVALID_BYTES,
        READ_INVALID_HEADER,
};

enum write_status
{
        WRITE_SUCCESS = 0,
        WRITE_INVALID_BYTES,
        WRITE_INVALID_HEADER
};

enum read_status from_bmp(FILE* file, struct image *img);

enum write_status to_bmp (FILE* file, struct image *img);

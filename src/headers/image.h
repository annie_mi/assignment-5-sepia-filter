#pragma once
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};
struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

void allocate_img(struct image *img, uint64_t w, uint64_t h);
struct image copy_img(struct image *img);
void clear_image(struct image *img);
